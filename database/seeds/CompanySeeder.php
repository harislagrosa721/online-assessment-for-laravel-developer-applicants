<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'name'          =>  'Purplebug Inc.',
                'email'         =>  'career@purplebug.net',
                'logo'          =>  '',
                'website'       =>  'https://purplebug.net/',
                'created_at'    =>  now(),
                'updated_at'    =>  now()
            ),
            array(
                'name'          =>  'Komodo Development Inc.',
                'email'         =>  'career@komodo.com',
                'logo'          =>  '',
                'website'       =>  'https://www.komodo.com/',
                'created_at'    =>  now(),
                'updated_at'    =>  now()
            ),
            array(
                'name'          =>  'Accenture',
                'email'         =>  'career@accenture.com',
                'logo'          =>  '',
                'website'       =>  'https://www.accenture.com/ph-en',
                'created_at'    =>  now(),
                'updated_at'    =>  now()
            ),
        );
        DB::table('companies')->insert($data);
    }
}
