<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'company_id'    =>  1,
                'first_name'    =>  'Haris',
                'last_name'     =>  'Lagrosa II',
                'email'         =>  'harislagrosa721@gmail.com',
                'phone'         =>  '09074603935',
                'created_at'    =>  now(),
                'updated_at'    =>  now()
            ),
        );
        DB::table('employees')->insert($data);
    }
}
