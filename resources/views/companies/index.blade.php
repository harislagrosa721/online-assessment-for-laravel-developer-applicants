@extends('layouts.master')
@section('title', 'Online Assessment for Laravel Developer Applicants')
@section('company', 'active')

@section('content')
    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Companies</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Companies</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header text-right">
                                <h3 class="card-title">Shows all the company details</h3>
                                <a href="{{ route('companies.create') }}" class="btn btn-sm btn-success">
                                    <span class="fa fa-plus"></span>
                                    Add new company
                                </a>
                            </div>
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Logo</th>
                                            <th>Website</th>
                                            <th class="text-center" style="width: 80px;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($companies as $company)
                                            <tr>
                                                <td class="text-capitalize">{{ $company->name }}</td>
                                                <td>{{ $company->email }}</td>
                                                <td>
                                                    <img src="{{ $company->logo }}" alt="{{ $company->logo }}" class="image-fluid">
                                                </td>
                                                <td>{{ $company->website }}</td>
                                                <td class="text-center">
                                                    <a href="{{ route('companies.edit', ['company' => $company->id]) }}" class="btn btn-primary">
                                                        <span class="fa fa-sm fa-edit"></span>
                                                    </a>
                                                    <a href="javascript:" id="company_{{ $company->id }}" class="btn btn-danger delete-company">
                                                        <span class="fa fa-sm fa-trash"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    @if (count($companies) > 10)
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Logo</th>
                                                <th>Website</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).on('click', '.delete-company', function () {
            var id = this.id.split('_')[1];
            if (confirm("Are you sure you want to delete this company?")) {
                $.ajax({
                    url:"/companies/"+id,
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data) {
                        if (data == 'success') {
                            location.reload()
                        }
                    },
                });
            }
        });
    </script>
@endsection
