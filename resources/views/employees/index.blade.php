@extends('layouts.master')
@section('title', 'Online Assessment for Laravel Developer Applicants')
@section('employee', 'active')

@section('content')
    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Employees</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Employees</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header text-right">
                                <h3 class="card-title">Shows all the employee details</h3>
                                <a href="{{ route('employees.create') }}" class="btn btn-sm btn-success">
                                    <span class="fa fa-plus"></span>
                                    Add new employee
                                </a>
                            </div>
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Company name</th>
                                            <th>First name</th>
                                            <th>last name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th class="text-center" style="width: 80px;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($employees as $employee)
                                            <tr>
                                                <td class="text-capitalize">{{ $employee->company->name }}</td>
                                                <td class="text-capitalize">{{ $employee->first_name }}</td>
                                                <td class="text-capitalize">{{ $employee->last_name }}</td>
                                                <td>{{ $employee->email }}</td>
                                                <td>{{ $employee->phone }}</td>
                                                <td>
                                                    <a href="{{ route('employees.edit', ['employee' => $employee->id]) }}" class="btn btn-primary">
                                                        <span class="fa fa-sm fa-edit"></span>
                                                    </a>
                                                    <a href="javascript:" id="employee_{{ $employee->id }}" class="btn btn-danger delete-employee">
                                                        <span class="fa fa-sm fa-trash"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    @if (count($employees) > 10)
                                        <tfoot>
                                            <tr>
                                                <th>Company name</th>
                                                <th>First name</th>
                                                <th>last name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                            </tr>
                                        </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).on('click', '.delete-employee', function () {
            var id = this.id.split('_')[1];
            if (confirm("Are you sure you want to delete this employee details?")) {
                $.ajax({
                    url:"/employees/"+id,
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data) {
                        if (data == 'success') {
                            location.reload()
                        }
                    },
                });
            }
        });
    </script>
@endsection
