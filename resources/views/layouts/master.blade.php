<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.includes.links')
</head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            @include('layouts.includes.top-navigation')

            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <a href="#" class="brand-link">
                    <img src="{{ asset('/storage/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                    <span class="brand-text font-weight-light">Online Assessment</span>
                </a>

                <div class="sidebar">
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                        <img src="{{ asset('/storage/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
                        </div>
                    </div>

                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                            <li class="nav-item">
                                <a href="{{ route('companies.index') }}" class="nav-link @yield('company')">
                                    <i class="nav-icon fas fa-th"></i>
                                    <p>Companies</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('employees.index') }}" class="nav-link @yield('employee')">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>Employees</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" class="nav-link @yield('employee')">
                                    <i class="nav-icon fas fa-sign-out-alt"></i>
                                    <p>Logout</p>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>

                        </ul>
                    </nav>
                </div>
            </aside>

            @yield('content')

            @include('layouts.includes.footer')
        </div>
        @include('layouts.includes.scripts')
        @yield('script')
    </body>
</html>

