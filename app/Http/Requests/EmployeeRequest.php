<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company'    =>  ['required', 'exists:companies,id'],
            'first_name'    =>  ['required', 'max:150'],
            'last_name'     =>  ['required', 'max:150'],
            'email'         =>  ['required', 'email', 'max:150', 'unique:employees,email,'.$this->id.',id', ],
            'phone'         =>  ['required', 'max:150', 'unique:employees,phone,'.$this->id.',id', ],
        ];
    }
}
